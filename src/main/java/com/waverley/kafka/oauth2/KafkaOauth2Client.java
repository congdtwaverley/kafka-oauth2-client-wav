package com.waverley.kafka.oauth2;

import com.waverley.kafka.oauth2.client.ConsumerOAuth;
import com.waverley.kafka.oauth2.client.ProducerOAuth;


public class KafkaOauth2Client {
    public static void main(String[] args) throws InterruptedException {
        ProducerOAuth producer = new ProducerOAuth();
        producer.publishMessage("default_topic", "transaction 4", "One billion dong to the the Orphan House");
        ConsumerOAuth consumer = new ConsumerOAuth();
        consumer.consumeMessage("default_topic");
    }
}
